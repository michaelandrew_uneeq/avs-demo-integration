const createError = require('http-errors')
const logger = require('../utils/logger')
const utils = require('../utils')

const tokenServices = require('../services/token')

const originValid = (origin, allowedOrigins) => {
    return allowedOrigins.split(',').find((allowedOrigin) => allowedOrigin === origin)
}

/**
 * This controller is bound to the API_ROUTE_TOKEN route defined in the
 * Express routes stack. The controller tries the 'getAccessToken' method of the
 * tokenServices service, and writes the resulting status and output to the Express
 * response object. Errors from the service are caught and terminated here
 * when the response object is written
 *
 * @param {*} req - Express request object
 * @param {*} res - Express response object
 * @param {*} next - Express next object
 */
let processRequest = async (req, res, next) => {
    logger.info(`New token request to ${req.originalUrl}`)

    try {
        const tokenBody = await tokenServices.getAccessToken()
        res.statusCode = 200
        const origins = process.env.TOKEN_ALLOWED_ORIGINS
        if (origins) {
            const origin = req.get('origin')
            if (originValid(origin, origins)) {
                res.setHeader('Access-Control-Allow-Origin', origin)
            }
            res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        }
        res.setHeader('Content-Type', 'application/json')
        res.write(tokenBody)
        res.send()
        logger.info('SENT: 200 OK: token sent')
    } catch (error) {
        logger.error(`getAccessToken failed: ${error.stack}`)
        res.status(500).send(createError(500))
        logger.info('SENT: 500 Internal Server Error')
    }
}

module.exports = { processRequest }
